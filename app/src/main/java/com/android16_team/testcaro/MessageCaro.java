package com.android16_team.testcaro;

public interface MessageCaro {
    public static final byte POSITION = 1;
    public static final byte MODE = 2;
    public static final byte MESSAGE = 3;
}
