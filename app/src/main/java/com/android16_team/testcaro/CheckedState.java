package com.android16_team.testcaro;

public enum CheckedState {
    O,
    X,
    NONE,
}
