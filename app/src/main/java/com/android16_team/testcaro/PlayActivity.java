package com.android16_team.testcaro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class PlayActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        int option=2; //0-play with bot, 1-play with friend

        final Intent callerIntent = getIntent();
        if(callerIntent != null) {
            Bundle callerBundle = callerIntent.getExtras();
            option = callerBundle.getInt("option");
        }
        if(option==0){ //play with bot
            Toast.makeText(this, "bot",Toast.LENGTH_SHORT).show();
            if (savedInstanceState == null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                PlayWithBot fragmentBot = new PlayWithBot();
                transaction.replace(R.id.content_fragment, fragmentBot);
                transaction.commit();
            }
        }
        else if (option ==1) //play with friend
        {
            Toast.makeText(this, "friend",Toast.LENGTH_SHORT).show();


            if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            CaroFragment fragment = new CaroFragment();
            transaction.replace(R.id.content_fragment, fragment);
            transaction.commit();
        }
        }
    }




}
