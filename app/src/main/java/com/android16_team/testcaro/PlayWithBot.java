package com.android16_team.testcaro;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class PlayWithBot extends Fragment {
    private DrawView drawView;
    private TextView txtCountDownTimer;
    private float cx,cy;
    private ImageButton btnMessage;
    private AlphaBeta bot;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.caro_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        drawView = view.findViewById(R.id.drawView);
        btnMessage = view.findViewById(R.id.btnMessage);
        bot = new AlphaBeta(30,3);
//        btnMessage.hide();

        drawView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                cx = event.getX();
                cy = event.getY();
                return false;
            }
        });

        drawView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = drawView.check(cx, cy);
                if (!drawView.isFinish() && msg != null) {//neu vi tri hop le
                    Node node = drawView.createNode(cx,cy);
                    drawView.setCheckedStates(node);
                    //check won?
                    if(drawView.isFinish()){
                        //win
                        Toast.makeText(view.getContext(),"win",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        drawView.clearNextStack();
                        // not win -> bot play
                        bot.setBoard(drawView.getCheckedStates());
                        Node nodes = new Node(0,0);
                        nodes = bot.getBestNode();
                        drawView.setCheckedStates(nodes);

                        if (drawView.isFinish()){ // neu khong danh duoc ma game da ket thuc co nghia la thua
                            //lose
                            Toast.makeText(view.getContext(),"lose",Toast.LENGTH_SHORT).show();
                        }

                    }
                    // clear the next stack
                }



            }
        });
        btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                startActivityForResult(intent, 123);
            }
        });

        txtCountDownTimer = view.findViewById(R.id.countTimer);
        CountDownTimer timer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                txtCountDownTimer.setText("" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {

            }
        }.start();


    }
}
