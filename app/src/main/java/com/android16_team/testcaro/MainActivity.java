package com.android16_team.testcaro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.nio.file.Files;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button play;
    Button exit;
    ImageButton bluetooth;
    ImageButton setUp;
    RadioButton playWithBot;
    RadioButton playWithFriend;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        play = findViewById(R.id.btn_play);
        exit = findViewById(R.id.btn_exit_main);
        bluetooth = findViewById(R.id.btn_bluetooth_main);
        setUp = findViewById(R.id.btn_setting_main);
        playWithBot = findViewById(R.id.radio_play_bot);
        playWithFriend = findViewById(R.id.radio_play_man);

        play.setOnClickListener(this);
        exit.setOnClickListener(this);
        setUp.setOnClickListener(this);
        bluetooth.setOnClickListener(this);

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_play:
                Intent myIntentA1A2 = new Intent(context, PlayActivity.class);
                Bundle myBundle1 = new Bundle();
                myBundle1.putInt("option", playWithBot.isChecked()==true?0:1);
                myIntentA1A2.putExtras(myBundle1);
                startActivity(myIntentA1A2);
                break;
            case R.id.btn_exit_main:
               finish();
                break;
            case R.id.btn_setting_main:
                Toast.makeText(this,"set up",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_bluetooth_main:
                Toast.makeText(this,"bluetooth",Toast.LENGTH_SHORT).show();
                break;
        }

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//            if ((resultCode == MainActivity.RESULT_OK)) {
//                Bundle myResultBundle = data.getExtras();
//                int exit = myResultBundle.getInt("exit");
//                if (exit == 1) finish();
//            }
//        }
//        catch(Exception e){
//
//        }
//    }
}
